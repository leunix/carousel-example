import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: 'evaluator-box',
    template: `<div class="slide-content">
        <div class="box" *ngIf="!removing"><h1>Multi evaluate box #{{ no }}</h1><button ion-button (click)="removeMe()">Remove</button></div>
        <div class="removing" *ngIf="removing">Removing!</div>
        </div>`
})
export class Box {
    @Input() no: number;
    @Output() onStartRemove: EventEmitter<null> = new EventEmitter();
    @Output() remove: EventEmitter<null> = new EventEmitter();

    removing: boolean = false;

    removeMe() {
        this.removing = true;
        window.setTimeout(() => this.remove.emit(), 1500);
    }
}