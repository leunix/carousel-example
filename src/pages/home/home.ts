import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NavController, Slides} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  public boxes: any[] = [];
  @ViewChild(Slides) slides: Slides;

  constructor(public elementRef: ElementRef, public navCtrl: NavController) {
    for(let i=0; i<6; i++) {
      this.boxes.push({ no: i });
    }
  }

  public ngOnInit() {
    window.setTimeout(() => {
        this.onSlideDidChange(1);
    }, 500);
  }

  public removeCurrent() {
    const index = this.slides.getActiveIndex();
    if(index === this.slides.length() - 2) {
      this.slides.slidePrev();
    } else {
      this.slides.slideNext();
    }

    window.setTimeout(() => {
      this.boxes.splice(index-1, 1);
      this.slides.update();

      if(this.boxes.length === 0) {
        console.log('No more boxes');
        return;
      }

      console.log(index, this.slides.length());
      if(index !== this.slides.length() - 2) {
          this.slides.slidePrev(0);
      } else {
        console.log('Confirm, at the last');
      }
      this.slides.lockSwipes(false);
    }, 1000);
  }

  public onSlideDidChange(speed: number = null) {
    const index = this.slides.getActiveIndex();
    if(index === 0) {
      window.setTimeout(() => this.slides.slideNext());
    } else if(index === this.slides.length() - 1) {
      window.setTimeout(() => this.slides.slidePrev());
    }
  }
}
